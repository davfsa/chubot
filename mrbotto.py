import discord
import asyncpg
import libneko
import typing
import os

from discord.ext import commands


class MrB(libneko.clients.Bot):
    def __init__(self, bot):
        self.bot = bot
        super().__init__(command_prefix=self.get_prefix, case_insensitive=True)

    async def on_start(self):
        self.owner = (await self.application_info()).owner
        if not hasattr(self, "db"):
            self.bot.db = await asyncpg.create_pool(
                user=os.environ["POSTGRES_USER"],
                password=os.environ["POSTGRES_PASSWORD"],
                database=os.environ["POSTGRES_DB"],
                host=os.getenv("POSTGRES_HOST", "localhost"),
                port=5432,
            )
            await self.bot.db.execute("""
                    CREATE TABLE IF NOT EXISTS prefixes (
                         guild_id    BIGINT    PRIMARY KEY,
                         prefix      VARCHAR(5),

                         CONSTRAINT valid_prefix_chk  CHECK  (TRIM(prefix) <> '')
                    );
                """)

    def get_cog(self, name: str) -> commands.Cog:  # will override the existing behaviour
        """Gets a cog by its name, ignoring casing."""
        for n in self.cogs:
            if n.casefold() == name.casefold():
                return self.cogs[n]

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        """Sets default prefix"""
        await self.db.execute("INSERT INTO prefixes (guild_id, prefix) VALUES ($1, $2);", guild.id, 'c!')

    async def get_prefix(self, message: discord.Message) -> typing.List[str]:
        """Gets a custom prefix for the given guild, or the default if in a DM."""
        default_prefix = 'c!'
        prefixes = []
        if not message.guild or message.author == self.owner:  # can easily set that property with application_info
            prefixes.append(default_prefix)
        else:
            try:
                prefixes.append(await self.db.fetchval("SELECT prefix FROM prefixes WHERE guild_id = $1",
                                                       message.guild.id) or default_prefix)
            except:
                prefixes.append(default_prefix)

        return commands.when_mentioned_or(prefixes)(self, message)
