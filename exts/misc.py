from discord.ext import commands
import datetime
import discord


class Misc(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.command(aliases=['info', 'bot', 'server', 'contribute', 'support'])  # Help command
    async def botinfo(self, ctx):
        """Displays a list of awesome people who deserve credit."""
        e = discord.Embed(
            title="Bot info:",
            description=f"The prefix for this server is: `{ctx.prefix}`\n\nFor support [click here](https://discord.gg/Z8KdkK5)!  \nWant to contribute [click here](https://gitlab.com/Chubonyo/chubot)!",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )

        await ctx.send(embed=e)

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.command(aliases=['credits'])  # Help command
    async def credit(self, ctx):
        """Displays a list of awesome people who deserve credit."""
        e = discord.Embed(
            title="A list of awesome people!",
            description="\n**nekoka#8788** for: teaching me most of what i know and helping with alot of the commands, and on top of all that hosting the bot!\n**davfsa#7026** for: helping alot with my learning and making a command that caused updates to become alot easier!\n**Forbidden#0001** for: writing a few commands and helping with alot, some not even bot related!\n**CorpNewt#4290** for: being one of the big reasons and inspirations for making a bot in the first place!\n\n**Special thanks to** [Discord Coding Academy](https://discord.gg/RDKfMcC), their staff team and bot developers for teaching me a lot, they were a huge help!",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        e.set_footer(text="Anyone who made it to this list is awesome!")

        await ctx.send(embed=e)

    @commands.cooldown(rate=1, per=3600, type=commands.BucketType.user)
    @commands.command()  # PatchNotes command
    async def patchnotes(self, ctx):
        """Displays the latest updates"""
        embed = discord.Embed(
            title="Chubot BETA V0.1.0",
            description="Out of date",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )

        await ctx.send(embed=embed)

    @commands.guild_only()
    @commands.cooldown(rate=5, per=3600, type=commands.BucketType.user)
    @commands.command()  # suggestion command
    async def suggest(self, ctx, *, suggestion: str = None):
        """Make a suggestion, sent directly to the owner.

        Do NOT abuse this, doing so will get you blacklisted.
        """
        if suggestion is None:
            await ctx.send("What would you like to suggest?")
        else:
            user = (await self.bot.application_info()).owner
            embed = discord.Embed(
                title=f"User: {ctx.message.author} ID: {ctx.author.id} suggested:",
                description=f"{suggestion}"[:500],
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            await user.send(embed=embed)
            embed = discord.Embed(
                description=f"Your suggestion has been sent {ctx.author.mention}",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            await ctx.send(embed=embed)
            await ctx.message.delete()


def setup(bot):
    bot.add_cog(Misc(bot))
