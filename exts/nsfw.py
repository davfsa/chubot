# I Chubonyo am not responsible for ANY of the content these commands may post

import aiohttp
import datetime
import discord
import random

from discord.ext import commands


class Nsfw(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.is_nsfw()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["blowjob", "suck", "succ"])
    async def bj(self, ctx):
        """WARNING: This command is 18+ ONLY!

        Blowjob gifs! Super lewd ;) :eyes:

        Sorts by hot on default but you can sort by hot, top and new"""

        choices = ["bj", "blowjob"]
        gif = random.choice(choices)
        async with aiohttp.request("get", f"https://nekos.life/api/v2/img/{gif}") as resp:
            nekos = await resp.json()

        embed = discord.Embed(
            title="Here's a bj ;) so lewd!",
            colour=0xFFCCFF
        )
        embed.set_footer(text="We are not responsible for any content this command may or may not post")
        embed.set_image(url=nekos["url"])
        await ctx.send(embed=embed)

    @bj.error
    async def on_error(self, ctx, error):
        if isinstance(error, commands.NSFWChannelRequired):
            return await ctx.send(f"{ctx.author.mention} This command can only be used in a NSFW channel.")
        
    @commands.is_nsfw()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['sex'])
    async def fuck(self, ctx, user: discord.Member = None):
        """WARNING: This command is 18+ ONLY!

        Fucks and does it hard!

        Sorts by hot on default but you can sort by hot, top and new"""
        if user is None:
            user = self.bot.user

        async with aiohttp.request("get", f"https://nekos.life/api/v2/img/classic") as resp:
            nekos = await resp.json()

        embed = discord.Embed(
            title=f"{ctx.author.display_name} fucks {user.display_name}!",
            colour=0xFFCCFF
        )
        embed.set_footer(text="We are not responsible for any content this command may or may not post")
        embed.set_image(url=nekos["url"])
        await ctx.send(embed=embed)

    @commands.is_nsfw()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['rhentai', 'r/hentai'])  # meme generator
    async def hentai(self, ctx, sort: str = 'hot'):
        """WARNING: This command is 18+ ONLY!

        Gets a post from r/hentai

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/Hentai/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                color=0xFFCCFF
            )
            embed.set_footer(text=f"We are not responsible for any of the content this command may or may not post")
            embed.set_image(url=post['url'])
            await ctx.send(embed=embed)

    @hentai.error
    async def on_error(self, ctx, error):
        if isinstance(error, commands.NSFWChannelRequired):
            return await ctx.send(f"{ctx.author.mention} This command can only be used in a NSFW channel.")


def setup(bot):
    bot.add_cog(Nsfw(bot))
