import aiohttp
import datetime
import discord
import random

from discord.ext import commands


class Tests(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.command(hidden=True)
    async def testmeme(self, ctx):
        async with aiohttp.request('get', 'https://www.reddit.com/r/dankmemes/new.json?sort=hot') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            print(post)
            embed = discord.Embed(
                title=post['title'],
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_image(url=post['url'])
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Tests(bot))
