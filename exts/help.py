from discord.ext import commands
import datetime
import discord



class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.prefix = 'c!'
        
    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.command(hidden=True) #Help command
    async def help(self, ctx):
        e = discord.Embed(
            title=":arrow_double_down:__**Bot Info**__:arrow_double_down:\n",
            description=f"Prefix for this server is: `{self.prefix}`\n\nThis bot was made by `Chubonyo#6969`\n\n:arrow_double_down:__**Commands**__:arrow_double_down:\n\n**Moderation**\n`ban`, `kick`, `purge`, `bans`\n\n**Utility**\n`uptime`, `ping`, `serverinfo`, `userinfo`, `convert`\n\n**Math**\n`add`, `minus`, `multiply`\n\n**Fun**\n`urban`, `meme`, `dankmeme`, `animeme`, `pewdsmeme`, `laddmeme`, `coolrate`, `gayrate`, `weebrate`, `howhigh`, `randomnumber`, `choose`, `roll`, `vs`, `echo`\n\n**Emotes**\n`what`, `sike`, `confused`, `cry`, `hug`, `dance`, `hmm`, `intel`\n\n**Misc**\n`suggest`, `credit`, `patchnotes`\n\n:arrow_double_down:__**Links**__:arrow_double_down:\n\n:link: [Official Server](https://discord.gg/ZdRdana)\n:link: **Bot Link:** *Currently Private*\n\n:arrow_double_down:__**Extra**__:arrow_double_down:\n\n`Hey you!` want to help with the development? dm __Chuboñyo™#6969__!\n**Want to suggest something?** use `c!suggest`",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        e.set_footer(text="Credit to CorpNewt & Nekoka.tt for all their help with the code")

        await ctx.send(embed=e)
 

def setup(bot):
    bot.add_cog(Help(bot))
