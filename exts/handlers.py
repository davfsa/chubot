import datetime
import logging
import os

import discord
import traceback

import libneko
from discord.ext import commands


logging.basicConfig(level=os.getenv("LOGGER_VERBOSITY", "INFO").upper())
logger = logging.getLogger("Chubonyo_Bot")


class Blacklisted(commands.CommandError):
    pass


def blacklist_check(ctx: commands.Context) -> bool:
    blacklist = (423439718030770180,
                 680835476034551925,
                 423439718030770180)
    if ctx.author.id in blacklist:
        raise Blacklisted()
    return True


class Listener(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.bot.add_check(blacklist_check)

    @commands.Cog.listener()  # Cooldown and blacklist handler
    async def on_command_error(self, ctx, error):
        error = error.__cause__ or error
        if isinstance(error, Blacklisted):
            blembed = discord.Embed(
                description="You are blacklisted from using Chuboñyo!\nif you believe this was wrongful please join the [official server](https://discord.gg/ZdRdana) for support",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            blembed.set_author(name=f"{ctx.author.display_name} you are blacklisted!", url=ctx.author.avatar_url,
                               icon_url=ctx.author.avatar_url)
            await ctx.send(embed=blembed)
            return
        if isinstance(error, commands.CommandOnCooldown):
            await ctx.send("Please wait `{seconds}` seconds before using the `{command}` command again.".format(
                command=ctx.command.name, seconds=round(error.retry_after, 2)))
            return
        else:
            user = (await self.bot.application_info()).owner

            @libneko.embed_generator(max_chars=700)
            def make_embed(paginator, page, index):
                em = discord.Embed(
                    description="".join(traceback.format_exception(type(error), error, error.__traceback__)),
                    timestamp=datetime.datetime.utcnow(),
                    color=0xFFCCFF
                )
                em.set_footer(text=f'Requested by {ctx.author.name}', icon_url=ctx.author.avatar_url)
                return em

            pag = libneko.EmbedNavigatorFactory(factory=make_embed, max_lines=30)
            for pg in pag.pages:
                await user.send(embed=pg)
            return


def setup(bot):
    bot.add_cog(Listener(bot))
