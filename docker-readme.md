Using this bot with Docker
==========================

Directory Structure
-------------------

Nest in a subdirectory.

```
chubot/
    chubonyo_bot/
        docker-readme.txt
        Dockerfile
        docker-compose.yml
        exts/...
        ...
    
    chubonyo_pgdata/
        ...
```

The `chubonyo_pgdata` will be generated when PostgreSQL starts.

Credentials
-----------

Make two files in `chubonyo_bot`.

`config.json`

```json
put a dummy example of your config file here.
```

`postgres.env`
```env
POSTGRES_USER=chubonyo
POSTGRES_PASSWORD=cipher6969420yeet!
POSTGRES_DB=chubot
```

Running
-------

Enter the `chubonyo_bot` directory, and run `docker-compose up`.
